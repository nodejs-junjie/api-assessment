
const PORT = process.env.PORT || 5000;

const DB_HOST = process.env.DB_HOST || 'localhost';
const DB_USER = process.env.DB_USER || 'root';
const DB_PASS = process.env.DB_PASS || 'password1';
const DB_NAME = process.env.DB_NAME || 'school';

exports.PORT = PORT;

exports.DB_HOST = DB_HOST;
exports.DB_USER = DB_USER;
exports.DB_PASS = DB_PASS;
exports.DB_NAME = DB_NAME;
