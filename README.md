# NodeJS API Assessment

NCS Development Team (Junjie)

[Gitlab Code Repository](https://gitlab.com/nodejs-junjie/api-assessment)

[Hosted API on Heroku Cloud (https://gentle-hollows-99168.herokuapp.com/)](https://gentle-hollows-99168.herokuapp.com/)

Instructions to run application locally:
* Use command `node server.js` to run the application
* Alternatively, `nodemon server.js` to run application using nodemon
* There is also 2 start scripts in `package.json` with `start` and `dev`
    * `start` is using node and `dev` is using nodemon

* For local MySQL database, update credentials in `config.js`.
    * Cloud app credentials will be using it's environment variables that I added
* Already have a sample script, which creates tables and sample data in `001-Creation of Entity and Relation Tables.sql`.
    * You may just run the tables creation scripts only with your own sample data. 

* Use `npm test` to run tests
