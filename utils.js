
const email_validator_regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

exports.email_validator_regex = email_validator_regex;
