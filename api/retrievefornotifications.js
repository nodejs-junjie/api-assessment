const express = require('express');
const db = require('../db');
const {email_validator_regex} = require('../utils');

const router = express.Router();

// API Number 4 - Notifications to students registered under teacher or @mentions if any

router.post('/', (req, res) => {
    const teacher = req.body.teacher;
    const notification = req.body.notification;

    let valid_teacher;

    if (teacher !== undefined && notification !== undefined) {
        // Check if teacher is valid
        let check_teacher_exists_sql = 'SELECT COUNT(*) AS total FROM teachers WHERE email = ?';

        db.query(check_teacher_exists_sql, teacher, (err, result) => {
            if (err) {
                throw err;
            }

            if (result[0].total === 0) {
                console.log(`teacher (${teacher}) does not exists..`);
                valid_teacher = false;
            } else {
                console.log(`teacher (${teacher}) exists...`);
                valid_teacher = true;
            }

            if (!valid_teacher) {
                return res.status(400).send({
                    message: `Teacher (${teacher}) is not found in the system...`

                });
            } else {
                let tokens = notification.split(' @');
                let additional_emails = [];

                for (let i = 0, len = tokens.length; i < len; i++) {
                    if (email_validator_regex.test(tokens[i])) {
                        additional_emails.push(tokens[i]);
                    }
                }

                if (additional_emails.length > 0) {
                    let check_additional_students_valid_sql =
                        'SELECT COUNT(*) as total FROM students WHERE email IN (?)'

                    db.query(check_additional_students_valid_sql, [additional_emails], (err, result) => {
                        if (err) {
                            throw err;
                        }

                        if (result[0].total === additional_emails.length) {
                            // Get all students registered with teacher
                            let get_students_registered_sql =
                                'SELECT email FROM students WHERE id IN (SELECT student_id FROM teachers_students_relation WHERE teacher_id = (SELECT id FROM teachers WHERE email = ?))';

                            db.query(get_students_registered_sql, teacher, (err, result) => {
                                if (err) {
                                    throw err;
                                }

                                let students = [];

                                if (result.length > 0) {
                                    for (let i = 0, len = result.length; i < len; i++) {
                                        students.push(result[i].email);
                                    }
                                }

                                for (let i = 0, len = additional_emails.length; i < len; i++) {
                                    students.push(additional_emails[i]);
                                }

                                // Get distinct emails
                                let get_student_emails_sql = 'SELECT email FROM students WHERE email IN (?)';

                                let distinct_students = [];

                                db.query(get_student_emails_sql, [students], (err, result) => {
                                    if (err) {
                                        throw err;
                                    }

                                    if (result.length > 0) {
                                        for (let i = 0, len = result.length; i < len; i++) {
                                            distinct_students.push(result[i].email);
                                        }
                                    }

                                    // Check if any student email is suspended, if none return json else error response
                                    let check_suspend_sql = 'SELECT COUNT(*) as total FROM students WHERE email IN (?) AND status = "suspend"';

                                    db.query(check_suspend_sql, distinct_students, (err, result) => {
                                        if (err) {
                                            throw err;
                                        }

                                        if (result[0].total === 0) {
                                            res.status(200).json({
                                                recipients: distinct_students
                                            })
                                        } else {
                                            res.status(400).send({
                                                message: 'One or more student(s) are suspended...'
                                            });
                                        }
                                    });
                                });
                            });
                        } else {
                            res.status(400).send({
                                message: 'Email(s) repeated or @mentions not found in system...'
                            });
                        }
                    });
                } else {
                    // Get all students registered with teacher
                    let get_students_registered_sql =
                        'SELECT email FROM students WHERE id IN (SELECT student_id FROM teachers_students_relation WHERE teacher_id = (SELECT id FROM teachers WHERE email = ?))';

                    db.query(get_students_registered_sql, teacher, (err, result) => {
                        if (err) {
                            throw err;
                        }

                        let students = [];

                        if (result.length > 0) {
                            for (let i = 0, len = result.length; i < len; i++) {
                                students.push(result[i].email);
                            }

                            // Check if students suspended, otherwise return json with student emails
                            let check_suspend_sql = 'SELECT COUNT(*) as total FROM students WHERE email IN (?) AND status = "suspend"';

                            db.query(check_suspend_sql, [students], (err, result) => {
                                if (err) {
                                    throw err;
                                }

                                if (result[0].total === 0) {
                                    res.status(200).json({
                                        recipients: students
                                    })
                                } else {
                                    res.status(400).send({
                                        message: 'One or more student(s) are suspended...'
                                    });
                                }
                            });
                        } else {
                            res.status(400).send({
                                message: 'Teacher has no students registered and no @mentions email...'
                            })
                        }
                    });
                }
            }
        });
    } else {
        res.status(400).send({
            message: 'Please check request, teacher and/or notifications are missing or invalid...'
        });
    }
});

module.exports = router;
