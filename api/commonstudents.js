const express = require('express');
const db = require('../db');

const router = express.Router();

// API Number 2 - Common students for ALL teacher(s)

router.get('/', (req, res) => {
    let teachers_sql = 'SELECT * FROM teachers WHERE email IN (?)';
    let result_array = [];
    let teachers;

    if (Array.isArray(req.query.teacher)) {
        teachers = req.query.teacher;

    } else {
        teachers = [req.query.teacher];
    }

    let data = [teachers];

    db.query(teachers_sql, data, (err, result) => {
        if (err) {
            throw err;
        }

        if (teachers.length === result.length) {
            let common_student_sql =
                'SELECT email FROM students ' +
                'WHERE id IN (SELECT student_id FROM teachers_students_relation ' +
                'WHERE teacher_id IN (SELECT id FROM teachers WHERE email IN ?) ' +
                'GROUP BY student_id ' +
                'HAVING COUNT(student_id) >= (SELECT COUNT(*) FROM teachers WHERE email IN ?))';

            db.query(common_student_sql, [data, data], (err, result) => {
                if (err) {
                    throw err;
                }

                for (let i = 0, len = result.length; i < len; i++) {
                    result_array.push(result[i].email);
                }

                res.status(200).json({
                    students: result_array
                });
            });
        } else {
            let valid_array = [];

            for (let i = 0, len = result.length; i < len; i++) {
                valid_array.push(result[i].email);
            }

            let invalid_emails = teachers
                .filter(f =>
                    !valid_array.includes(f));

            if (invalid_emails.length > 0 && invalid_emails[0] !== undefined) {
                res.status(400).send({
                    message: `The teacher(s) [${invalid_emails}] does not exists...`
                });
            } else {
                res.status(400).send({
                    message: `The request's information is not valid, please check...`
                });
            }
        }
    });
});

module.exports = router;
