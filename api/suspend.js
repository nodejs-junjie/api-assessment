const express = require('express');
const db = require('../db');

const router = express.Router();

// API Number 3 - Suspend student

router.post('/', (req, res) => {
    let sql_select = 'SELECT id FROM students WHERE email = ?';
    let sql_update_suspend = 'UPDATE students SET status = "suspend" WHERE email = ?';

    let req_email = req.body.student;

    if (req_email !== undefined) {
        db.query(sql_select, req_email, (err, result) => {
            if (err) {
                throw err;
            }

            if (result.length === 1) {
                db.query(sql_update_suspend, req_email, (err, result) => {
                    if (err) {
                        throw err;
                    }
                });
                res.status(204).end();
            } else if (result.length === 0) {
                res.status(400).send({
                    message: `The student (${req_email}) is not found...`
                });
            } else {
                res.status(400).send({
                    message: `The student (${req_email}) has multiple records, please check data is valid...`
                });
            }
        });
    } else {
        res.status(400).send({
            message: 'Please check request, invalid or missing student email...'
        });
    }
});

module.exports = router;
