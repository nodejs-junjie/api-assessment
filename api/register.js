const express = require('express');
const db = require('../db');
const {email_validator_regex} = require('../utils');

const router = express.Router();

// API Number 1 - Register teacher and students, create if either does not exists

router.post('/', (req, res) => {
    let teacher = req.body.teacher;
    let students;

    if (Array.isArray(req.body.students)) {
        students = req.body.students;
    } else {
        students = [req.body.students];
    }

    if (teacher !== undefined && students[0] !== undefined) {
        let valid_student_email = false;

        for (let i = 0, len = students.length; i < len; i++) {
            if (email_validator_regex.test(students[i])) {
                valid_student_email = true;
            } else {
                valid_student_email = false;
                break;
            }
        }
        if (!valid_student_email) {
            res.status(400).send({
                message: 'Student email is in invalid format, please check...'
            });
        } else if (!email_validator_regex.test(teacher)) {
            res.status(400).send({
                message: 'Teacher email is in invalid format, please check...'
            });
        } else {
            let check_teacher_exists_sql = 'SELECT COUNT(*) AS total FROM teachers WHERE email = ?';

            // Check teacher exists, if not create
            db.query(check_teacher_exists_sql, teacher, (err, result) => {
                if (err) {
                    throw err;
                }

                if (result[0].total === 0) {
                    let add_teacher_sql = 'INSERT INTO teachers (email) VALUES (?)';

                    db.query(add_teacher_sql, teacher, (err, result) => {
                        if (err) {
                            throw err;
                        }

                        console.log(`teacher (${teacher}) does not exists, successfully created in database!`);
                    });
                } else {
                    console.log(`teacher (${teacher}) exists...`);
                }
            });


            // For each student, check if exists. If not, create student with status active as default

            let check_student_exists_sql = 'SELECT COUNT(*) AS total FROM students WHERE email = ?';

            for (let i = 0, len = students.length; i < len; i++) {
                db.query(check_student_exists_sql, students[i], (err, result) => {
                    if (err) {
                        throw err;
                    }

                    if (result[0].total === 0) {
                        let add_student_sql = 'INSERT INTO students (email, status) VALUES (?, "active")';

                        db.query(add_student_sql, students[i], (err, result) => {
                            if (err) {
                                throw err;
                            }

                            console.log(`student (${students[i]}) does not exists, successfully created in database!`);
                        })
                    } else {
                        console.log(`student (${students[i]}) exists...`);
                    }
                });
            }

            // Check if each student is already registered, otherwise register in relation table

            let check_relation_sql =
                'SELECT COUNT(*) as total FROM teachers_students_relation ' +
                'WHERE teacher_id = (SELECT id FROM teachers WHERE email = ?) ' +
                'AND student_id = (SELECT id FROM students WHERE email = ?)';

            for (let i = 0, len = students.length; i < len; i++) {
                db.query(check_relation_sql, [teacher, students[i]], (err, result) => {
                    if (err) {
                        throw err;
                    }

                    if (result[0].total === 0) {
                        let add_relation_sql =
                            'INSERT INTO teachers_students_relation (teacher_id, student_id) ' +
                            'VALUES ((SELECT id FROM teachers WHERE email = ?), ' +
                            '(SELECT id FROM students WHERE email = ?))';

                        db.query(add_relation_sql, [teacher, students[i]], (err, result) => {
                            if (err) {
                                throw err;
                            }

                            console.log(`student (${students[i]}) successfully registered under teacher (${teacher})!`);
                        })
                    } else {
                        console.log(`student (${students[i]}) already registered to teacher (${teacher})...`);
                    }
                });
            }
            res.status(204).end();
        }
    } else {
        res.status(400).send({
            message: 'Please check request, teacher and/or students are missing or invalid...'
        });
    }
});

module.exports = router;
