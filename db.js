const mysql = require('mysql');
const {DB_HOST, DB_NAME, DB_USER, DB_PASS} = require('./config');

const db_config = {
    host: DB_HOST,
    user: DB_USER,
    password: DB_PASS,
    database: DB_NAME
}

// To handle mysql disconnections

let db;

function handleDisconnect() {
    db = mysql.createConnection(db_config);

    db.connect(function (err) {
        if (err) {
            console.log('error when connecting to db:', err);
            setTimeout(handleDisconnect, 2000);
        }
        console.log('Database connected...');
    });

    db.on('error', function (err) {
        console.log('db error', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            handleDisconnect();
        } else {
            throw err;
        }
    });
}

handleDisconnect();

module.exports = db;
