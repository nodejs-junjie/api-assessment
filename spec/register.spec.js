const request = require('request');

const {PORT} = require('../config');

const register_API = `http://localhost:${PORT}/api/register`;

describe('Register API', () => {
    let server;

    beforeAll(() => {
        server = require('../app');
    });

    describe('POST /api/register', () => {

        describe('no data sent', () => {
            it('should return status 400', (done) => {
                request.post(register_API, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message', (done) => {
                request.post(register_API, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe('Please check request, teacher and/or students are missing or invalid...');
                    done();
                });
            });
        });

        describe('missing json key', () => {

            it('should return status 400', (done) => {
                request.post({url: register_API, form: {students: 'invalid_1'}}, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message', (done) => {
                request.post({url: register_API, form: {students: 'invalid_1'}}, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe('Please check request, teacher and/or students are missing or invalid...');
                    done();
                });
            });
        });

        describe('invalid json key', () => {

            it('should return status 400', (done) => {
                request.post({
                    url: register_API,
                    form: {teachers: 'invalid_teacher_key', students: ['invalid_1', 'invalid_2']}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message', (done) => {
                request.post({
                    url: register_API,
                    form: {teachers: 'invalid_teacher_key', students: ['invalid_1', 'invalid_2']}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe('Please check request, teacher and/or students are missing or invalid...');
                    done();
                });
            });
        });

        describe('invalid teacher email format', () => {
            const invalid_email_format = 'invalid';

            it('should return status 400', (done) => {
                request.post({
                    url: register_API,
                    form: {teacher: invalid_email_format, students: 'student@mail.com'}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message', (done) => {
                request.post({
                    url: register_API,
                    form: {teacher: invalid_email_format, students: 'student@mail.com'}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe('Teacher email is in invalid format, please check...');
                    done();
                });
            });
        });

        describe('invalid student email format', () => {
            const invalid_email_format = 'invalid';

            it('should return status 400', (done) => {
                request.post({
                    url: register_API,
                    form: {teacher: 'teacher@mail.com', students: invalid_email_format}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message', (done) => {
                request.post({
                    url: register_API,
                    form: {teacher: 'teacher@mail.com', students: invalid_email_format}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe('Student email is in invalid format, please check...');
                    done();
                });
            });
        });

        describe('valid teacher and student email', () => {
            const valid_teacher_email = 'teacher_new_test@school.com';
            const valid_student_email = 'student_new_test@school.com';

            it('should return status 204', (done) => {
                request.post({
                    url: register_API,
                    form: {teacher: valid_teacher_email, students: valid_student_email}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(204);
                    done();
                });
            });
        });

    });

});
