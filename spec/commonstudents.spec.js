const request = require('request');

const {PORT} = require('../config');

const commonstudents_API = `http://localhost:${PORT}/api/commonstudents`;

describe('Common Students API', () => {
    let server;

    beforeAll(() => {
        server = require('../app');
    });

    describe('GET /api/commonstudents', () => {

        describe('no data sent', () => {
            it('should return status 400', (done) => {
                request.get(commonstudents_API, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message', (done) => {
                request.get(commonstudents_API, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe('The request\'s information is not valid, please check...');
                    done();
                });
            });
        });

        describe('invalid query parameters', () => {
            const invalid_request_URL = commonstudents_API + '?invalid=invalid@mail.com';

            it('should return status 400', (done) => {
                request.get(invalid_request_URL, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message', (done) => {
                request.get(invalid_request_URL, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe('The request\'s information is not valid, please check...');
                    done();
                });
            });
        });

        describe('single invalid email', () => {
            const invalid_email = 'invalid@mail.com';
            const invalid_request_URL = commonstudents_API + '?teacher=' + invalid_email;

            it('should return status 400', (done) => {
                request.get(invalid_request_URL, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message with invalid email', (done) => {
                request.get(invalid_request_URL, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe(`The teacher(s) [${invalid_email}] does not exists...`);
                    done();
                });
            });
        });

        describe('single valid email', () => {
            const valid_email = 'teacher_sansa@school.com';
            const valid_request_URL = commonstudents_API + '?teacher=' + valid_email;

            it('should return status 200', (done) => {
                request.get(valid_request_URL, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(200);
                    done();
                });
            });

            it('should return common students json', (done) => {
                request.get(valid_request_URL, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    const valid_response = ['student_robb@school.com'];

                    expect(JSON.parse(body).students).toEqual(valid_response);
                    done();
                });
            });
        });

        describe('multiple valid email', () => {
            const valid_email_1 = 'teacher_ken@school.com';
            const valid_email_2 = 'teacher_jon@school.com';
            const valid_request_URL = commonstudents_API + '?teacher=' + valid_email_1 + '&teacher=' + valid_email_2;

            it('should return status 200', (done) => {
                request.get(valid_request_URL, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(200);
                    done();
                });
            });

            it('should return common students json', (done) => {
                request.get(valid_request_URL, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    const valid_response = ['student_ned@school.com'];

                    expect(JSON.parse(body).students).toEqual(valid_response);
                    done();
                });
            });
        });
    });

});
