const request = require('request');

const {PORT} = require('../config');

const base_URL = `http://localhost:${PORT}/`;

describe('App Server', () => {
    let server;

    beforeAll(() => {
        server = require('../app');
    });

    describe('GET /', () => {
        it('should return status 400', (done) => {
            request.get(base_URL, (error, response, body) => {
                if (error) {
                    throw error;
                }

                expect(response.statusCode).toBe(400);
                done();
            });
        });

        it('should return error message', (done) => {
            request.get(base_URL, (error, response, body) => {
                if (error) {
                    throw error;
                }

                expect(JSON.parse(body).message).toBe('Please use a valid GET API');
                done();
            });
        });
    });

    describe('POST /', () => {
        it('should return status 400', (done) => {
            request.post(base_URL, (error, response, body) => {
                if (error) {
                    throw error;
                }

                expect(response.statusCode).toBe(400);
                done();
            });
        });

        it('should return error message', (done) => {
            request.post(base_URL, (error, response, body) => {
                if (error) {
                    throw error;
                }

                expect(JSON.parse(body).message).toBe('Please use a valid POST API');
                done();
            });
        });
    });
});
