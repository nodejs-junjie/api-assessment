const request = require('request');

const {PORT} = require('../config');

const suspend_API = `http://localhost:${PORT}/api/suspend`;

describe('Suspend API', () => {
    let server;

    beforeAll(() => {
        server = require('../app');
    });

    describe('POST /api/suspend', () => {

        describe('no data sent', () => {
            it('should return status 400', (done) => {
                request.post(suspend_API, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message', (done) => {
                request.post(suspend_API, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe('Please check request, invalid or missing student email...');
                    done();
                });
            });
        });

        describe('invalid json key', () => {

            it('should return status 400', (done) => {
                request.post({url: suspend_API, form: {students: 'invalid'}}, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message', (done) => {
                request.post({url: suspend_API, form: {students: 'invalid'}}, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe('Please check request, invalid or missing student email...');
                    done();
                });
            });
        });

        describe('email not exists', () => {
            const invalid_email = 'invalid@mail.com';

            it('should return status 400', (done) => {
                request.post({url: suspend_API, form: {student: invalid_email}}, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message', (done) => {
                request.post({url: suspend_API, form: {student: invalid_email}}, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe(`The student (${invalid_email}) is not found...`);
                    done();
                });
            });
        });

        describe('email is valid', () => {
            const valid_email = 'student_arya@school.com';

            it('should return status 204', (done) => {
                request.post({url: suspend_API, form: {student: valid_email}}, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(204);
                    done();
                });
            });
        });

    });

});
