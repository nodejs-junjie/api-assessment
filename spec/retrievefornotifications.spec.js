const request = require('request');

const {PORT} = require('../config');

const retrievefornotifications_API = `http://localhost:${PORT}/api/retrievefornotifications`;

describe('Retrieve for Notifications API', () => {
    let server;

    beforeAll(() => {
        server = require('../app');
    });

    describe('POST /api/retrievefornotifications', () => {

        describe('no data sent', () => {
            it('should return status 400', (done) => {
                request.post(retrievefornotifications_API, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message', (done) => {
                request.post(retrievefornotifications_API, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe('Please check request, teacher and/or notifications are missing or invalid...');
                    done();
                });
            });
        });

        describe('missing json key', () => {

            it('should return status 400', (done) => {
                request.post({
                    url: retrievefornotifications_API,
                    form: {teacher: 'invalid_1'}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message', (done) => {
                request.post({
                    url: retrievefornotifications_API,
                    form: {teacher: 'invalid_1'}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe('Please check request, teacher and/or notifications are missing or invalid...');
                    done();
                });
            });
        });

        describe('invalid json key', () => {

            it('should return status 400', (done) => {
                request.post({
                    url: retrievefornotifications_API,
                    form: {teachers: 'invalid_key', notification: 'invalid'}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message', (done) => {
                request.post({
                    url: retrievefornotifications_API,
                    form: {teachers: 'invalid_key', notification: 'invalid'}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe('Please check request, teacher and/or notifications are missing or invalid...');
                    done();
                });
            });
        });

        describe('teacher email not found', () => {
            const invalid_email = 'invalid_email@mail.com';

            it('should return status 400', (done) => {
                request.post({
                    url: retrievefornotifications_API,
                    form: {teacher: invalid_email, notification: 'invalid'}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message', (done) => {
                request.post({
                    url: retrievefornotifications_API,
                    form: {teacher: invalid_email, notification: 'invalid'}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe(`Teacher (${invalid_email}) is not found in the system...`);
                    done();
                });
            });
        });

        describe('repeated email in @mentions', () => {
            const teacher_email = 'teacher_ken@school.com';
            const repeated_notification = 'Hello students! @student_joe@school.com @student_joe@school.com';

            it('should return status 400', (done) => {
                request.post({
                    url: retrievefornotifications_API,
                    form: {teacher: teacher_email, notification: repeated_notification}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message', (done) => {
                request.post({
                    url: retrievefornotifications_API,
                    form: {teacher: teacher_email, notification: repeated_notification}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe('Email(s) repeated or @mentions not found in system...');
                    done();
                });
            });
        });

        describe('email in @mentions not found', () => {
            const teacher_email = 'teacher_ken@school.com';
            const not_found_notification = 'Hello students! @student_joeeeee@school.com';

            it('should return status 400', (done) => {
                request.post({
                    url: retrievefornotifications_API,
                    form: {teacher: teacher_email, notification: not_found_notification}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(400);
                    done();
                });
            });

            it('should return error message', (done) => {
                request.post({
                    url: retrievefornotifications_API,
                    form: {teacher: teacher_email, notification: not_found_notification}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).message).toBe('Email(s) repeated or @mentions not found in system...');
                    done();
                });
            });
        });

        describe('valid teacher email and with valid @mentions', () => {
            const teacher_email = 'teacher_sansa@school.com';
            const valid_notification = 'Hello students! @student_joe@school.com';

            const valid_response = ['student_robb@school.com', 'student_joe@school.com'];

            it('should return status 200', (done) => {
                request.post({
                    url: retrievefornotifications_API,
                    form: {teacher: teacher_email, notification: valid_notification}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(response.statusCode).toBe(200);
                    done();
                });
            });

            it('should return recipients list in json', (done) => {
                request.post({
                    url: retrievefornotifications_API,
                    form: {teacher: teacher_email, notification: valid_notification}
                }, (error, response, body) => {
                    if (error) {
                        throw error;
                    }

                    expect(JSON.parse(body).recipients.sort()).toEqual(valid_response.sort());
                    done();
                });
            });
        });

    });

});
