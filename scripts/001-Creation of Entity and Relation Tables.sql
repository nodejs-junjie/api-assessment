-- Create tables and relation table

-- use school;

drop table if exists teachers_students_relation;

drop table if exists teachers;

drop table if exists students;

create table teachers (
  id int auto_increment,
  email varchar(255),
  primary key (id)
);

create table students (
  id int auto_increment,
  email varchar(255),
  status varchar(20),
  primary key (id)
);

create table teachers_students_relation (
  teacher_id int not null,
  student_id int not null,
  primary key (teacher_id, student_id),
  foreign key (teacher_id) references teachers (id),
  foreign key (student_id) references students (id)
);


-- Create sample data for teachers and student

insert into teachers (id, email) values (1, 'teacher_ken@school.com');
insert into teachers (id, email) values (2, 'teacher_jon@school.com');
insert into teachers (id, email) values (3, 'teacher_emily@school.com');
insert into teachers (id, email) values (4, 'teacher_sansa@school.com');
insert into teachers (id, email) values (5, 'teacher_bran@school.com');

insert into students (id, email, status) values (1, 'student_joe@school.com', 'active');
insert into students (id, email, status) values (2, 'student_robert@school.com', 'active');
insert into students (id, email, status) values (3, 'student_ned@school.com', 'active');
insert into students (id, email, status) values (4, 'student_robb@school.com', 'active');
insert into students (id, email, status) values (5, 'student_arya@school.com', 'active');


-- Create sample relation data for teachers and students

insert into teachers_students_relation (teacher_id, student_id) values (1, 1);
insert into teachers_students_relation (teacher_id, student_id) values (1, 3);
insert into teachers_students_relation (teacher_id, student_id) values (1, 4);
insert into teachers_students_relation (teacher_id, student_id) values (2, 2);
insert into teachers_students_relation (teacher_id, student_id) values (2, 3);
insert into teachers_students_relation (teacher_id, student_id) values (3, 4);
insert into teachers_students_relation (teacher_id, student_id) values (3, 5);
insert into teachers_students_relation (teacher_id, student_id) values (4, 4);
insert into teachers_students_relation (teacher_id, student_id) values (5, 3);
insert into teachers_students_relation (teacher_id, student_id) values (5, 1);


