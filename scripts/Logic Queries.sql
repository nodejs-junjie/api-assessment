
-- API Number 1 - Create ONE/MANY students for ONE teacher -

-- Input 'email' to check if teacher exist
select count(*) from teachers where email = 'teacher_jon@school.com';

-- Insert new record if not exists
insert into teachers (email)
values ('teacher_new@school.com');



-- For each student, check if exist otherwise create student (code logic NOT sql)

-- Input 'email' to check if teacher exist
select count(*) from students where email = 'student_arya@school.com';

-- Insert new record if not exists
insert into students (email)
values ('student_new@school.com');



-- Check if teacher student relation exists
select student_id from teachers_students_relation
where teacher_id = (
  select id from teachers where email = 'teacher_jon@school.com'
)
  and student_id in (
  select id from students where email in (
                                          'student_arya@school.com', 'student_ned@school.com'
    )
)
group by student_id;


-- (At least ONE is registered) If not ALL students are registered, check who is NOT registered
select email from students where id not in (3) and email in ('student_arya@school.com', 'student_ned@school.com');

-- Insert record ONE at a time for each student not registered
insert into teachers_students_relation (teacher_id, student_id)
values ((select id from teachers where email = 'teacher_jon@school.com'),
        (select id from students where email = 'student_arya@shool.com'));


-- If NONE of students is registered, insert records to relation table
insert into teachers_students_relation (teacher_id, student_id)
values ((select id from teachers where email = 'teacher_jon@school.com'),
        (select id from students where email = 'student_arya@shool.com')),
       ((select id from teachers where email = 'teacher_jon@school.com'),
        (select id from students where email = 'student_ned@shool.com'));










-- API Number 2 - Get COMMON students from ALL teachers email - Input 'email' field from user request

select student_id from teachers_students_relation where teacher_id in (
  select id
  from teachers
  where email IN ('teacher_ken@school.com', 'teacher_jon@school.com', 'teacher_bran@school.com')
)
group by student_id
having count(student_id) =
       (select count(*) from teachers
        where email in ('teacher_ken@school.com', 'teacher_jon@school.com', 'teacher_bran@school.com'));








-- API Number 3 - Update student status to 'suspend' based on ONE student email

-- Check email exists, if not return error message
select id from students where email = 'student_arya@school.com';

-- Update only if exists
update students
set status = 'suspend'
where email = 'student_arya@school.com';







-- API Number 4 - Get recipients (students) to receive notifications

-- (Code logic NOT sql) To extract any @mentions emails if any. Get students under the teacher (defaulted to receive)
-- Additional notifications to send to emails in @mentions email.
-- Check if student is suspended, if so do not send notification.
-- Response should NOT have any duplicate emails

-- Check teacher's existence
select id from teachers where email = 'teacher_bran@school.com';

-- If teacher is valid, check all students under teacher and NOT suspend
select email from students
where id in
      (select student_id from teachers_students_relation
       where teacher_id = (select id from teachers where email = 'teacher_bran@school.com'))
  and
    status != 'suspend';



-- If @mentions emails are available, check if all is valid.
-- (Code logic) filter off duplicates first before query database

select count(*) from students where email in ('student_arya@school.com', 'student_ned@school.com');

select distinct email from students where email in ('student_arya@school.com', 'student_arya@school.com', 'student_ned@school.com');
select distinct email from students where id in (1, 1, 3, 4, 4, 5);




