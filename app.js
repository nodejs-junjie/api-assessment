const express = require('express');
const bodyParser = require('body-parser');
const app = express();

//const apiRoutes = require('./api/api');

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

//app.use('/api/api', apiRoutes);

app.get('/', (req, res) => {
    res.status(400).send({
        message: 'Please use a valid GET API'
    })
});

app.post('/', (req, res) => {
    res.status(400).send({
        message: 'Please use a valid POST API'
    })
});

// Register API
app.use('/api/register', require('./api/register'));

// Common Students API
app.use('/api/commonstudents', require('./api/commonstudents'));

// Suspend API
app.use('/api/suspend', require('./api/suspend'));

// Retrieve for Notifications API
app.use('/api/retrievefornotifications', require('./api/retrievefornotifications'));

module.exports = app;
